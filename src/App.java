import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class App {

    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        int sum100 = sumNumbersV1();
        System.out.println("sum100 = " + sum100);

        int[] number1 = {1, 5 , 10};
        int[] number2 = {1, 2 ,3, 5 ,7 ,8};

        System.out.println("number1 = " + number1);
        System.out.println("number2 = " + number2);

        
        printHello(99);


        System.out.println("subtask 1 50.10");

        isArray();

        
        System.out.println("subtask 2 50.20");

        int[] arr = {1,2,3,4,5,6};

        nArray(arr, 5);

        System.out.println("subtask 3 50.20");

        int[] sapXepArr = {3,8,7,6,5,-4,-3,2,1};
        sapXepMang(sapXepArr);

        System.out.println("subtask 4 50.20");

        int[] thuTuSArray = {1,2,3,4,5,6};

        soThuTuArray(thuTuSArray, 4);

        System.out.println("subtask 5 50.20");

        int[] array1 = {1,2,3};
        int[] array2 = {4,5,6};

        gopMang(array1, array2);

        System.out.println("subtask 6 50.30");

        inHoaChuCaiDau("js string exercises");

        System.out.println("subtask 7 50.30");

        lapLaiChuoi("ha!", 6);

        System.out.println("subtask 8 50.30");

        stringToArray("dcresource", 2);



        System.out.println("subtask 3 50.40");

        int[] arrayTrung = {1,2,1,4,5,1,1,3,1};

        timPhanTuTrung(arrayTrung);


        System.out.println("subtask 4 50.40");

        int[] arrayTong = {1,2,3,4,5,6};

        tinhTongPhanTuMang((arrayTong));

        System.out.println("subtask 1 50.50");


        diemSoLanXuatHien("DCresource: JavaScript Exercises", 'e');


        
        System.out.println("subtask 2 50.50");

        loaiBoKiTuTrang(" dcresource    ");
        
    }

    public static int sumNumbersV1 () {
        int sum = 0;
        for (int i = 0; i < 100; i++) {
            sum = sum + i;
        }
        return sum;
    }

    public static int sumNumbersV2 (int[] arrayNumber) {
        int sum = 0;

        for (int i = 0; i < arrayNumber.length; i++) {
          sum = sum + arrayNumber[i];  
        }

        return sum;
    }

    public static void printHello(int number) {
        if(number <= 0) {
            System.out.println("Hello, World!");
        }else {
            int du = number % 2;

            if (du == 0){
                System.out.println("Day la so chan");
            }else {
                System.out.println("Day la so le");
            }
        }
    }

    public static void isArray() {
        String str = "Devcamp";
        if(str instanceof String) {
            System.out.println("Đây không phải là mảng");
        }else {
            System.out.println("Đây  là mảng");
        }

        int[] i = {1,2,3};
        if(i instanceof int[]) {
            System.out.println("Đây  là mảng");
            }else {
                System.out.println("Đây không phải là mảng");
            }
         
    }


    public static void nArray(int[] arr , int n) {
        if(n > arr.length) {
            System.out.println("Không tìm thấy số này");
        }else {
            System.out.println(arr[n]);
        }
    }

    public static void sapXepMang(int [] arr) {
        int temp = arr[0];
        for (int i = 0 ; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                }
            }
        }
        System.out.println(arr);
    }


    public static void soThuTuArray(int[] arr , int n) {
        int a = -1;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] == n){
                a = i;

            }
        }
        System.out.println(a);
    }

    public static void gopMang(int[] array1 , int[] array2) {
        int length = array1.length + array2.length;

        int[] result = new int[length];
        int pos = 0;

        for (int element : array1) {
          result[pos] = element;
          pos++;
        }

        for (int element : array2) {
          result[pos] = element;
          pos++;
        }

        System.out.println(result);
    }


    public static void inHoaChuCaiDau(String name) {
        String firstLetter = name.substring(0, 1);
        // chuỗi remainingLetters chứa phần còn lại của name
        String remainingLetters = name.substring(1, name.length());
     
        //sử dụng phương thức toUpperCase() để chuyển đổi firstLetter thành chữ in hoa
        firstLetter = firstLetter.toUpperCase();
     
        //sau khi chuyển đổi thì gộp chúng lại
        name = firstLetter + remainingLetters;
        System.out.println("Chuỗi sau khi đổi: " + name);
    }

    public static void lapLaiChuoi(String name, int n) {
        String resultName = "";
        for (int i = 1; i <= n; i++) {
            resultName = resultName + " " + name;
        }
        System.out.println("Chuỗi sau khi đổi: " + resultName.trim());
    }

    public static void stringToArray(String line, int n) {
        String Array = "";
        for (int i = 0; i < line.length(); i= i + n) {
            if(i+n < line.length() ) {
                Array = Array + " " + line.substring(i, i + n); 
            }else {
                Array = Array + " " + line.substring(i);
            }
            
        }
        String[] words = Array.trim().split(" ");
        System.out.println("Chuỗi sau khi đổi: " + words);
    }

    public static int removeDuplicateElements(int arr[], int n){  
        if (n==0 || n==1){  
            return n;  
        }  
        int[] temp = new int[n];  
        int j = 0;  
        for (int i=0; i<n-1; i++){  
            if (arr[i] != arr[i+1]){  
                temp[j++] = arr[i];  
            }  
         }  
        temp[j++] = arr[n-1];     
         
        for (int i=0; i<j; i++){  
            arr[i] = temp[i];  
        }  
        return j;  
    }  
        
    public static void addElement(Map<Integer, Integer> map, int element) {
        if (map.containsKey(element)) {
            int count = map.get(element) + 1;
            map.put(element, count);
        } else {
            map.put(element, 1);
        }
    }  

    public static void timPhanTuTrung(int[] arr){
                // tìm số lần xuất hiện của các phần tử
                Map<Integer, Integer> map = new TreeMap<Integer, Integer>();
                for (int i = 0; i < arr.length; i++) {
                    addElement(map, arr[i]);
                }
                System.out.print("Các phần tử xuất hiện 2 lần: \n");
                for (Integer key : map.keySet()) {
                    System.out.printf("%d xuất hiện %d lần.\n", key, map.get(key));
                }
    }


    public static void tinhTongPhanTuMang(int[] array) {
        int sum = 0;
        //Advanced for loop
        for( int num : array) {
            sum = sum+num;
        }
        System.out.println("Kết quả là:"+sum);
    }


      public static void diemSoLanXuatHien(String chuoi,char kyTu) {

        int count = 0;
        String Chuoi = chuoi.toLowerCase();
        for (int i = 0; i < chuoi.length(); i++) {
            // Nếu ký tự tại vị trí thứ i bằng 'a' thì tăng count lên 1
            if (Chuoi.charAt(i) == kyTu) {
                count++;
            }
        }

        System.out.println("Số lần xuất hiện của ký tự " + kyTu +
        " trong chuỗi " + chuoi + " = " + count);
      }

      public static void loaiBoKiTuTrang(String chuoi) {
        String Chuoi = chuoi.trim();

        System.out.println(Chuoi);
      }
}
